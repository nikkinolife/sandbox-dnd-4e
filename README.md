# Sandbox DnD 4e

## WARNING

It is not a "full power" standalone system. It is created in Sandbox System Builder - https://gitlab.com/rolnl/sandbox-system-builder

## How to install?

1. Go to your Foundry settings and install Sandbox System Builder. 
2. Go to Worlds tab, press Install World, copy-paste this link and press Install.

`https://gitlab.com/nikkinolife/sandbox-dnd-4e/-/raw/master/world.json`

(or you can just download .zip file and throw it in worlds folder)

## How to use?

There are two types of Actors - (N)PC character sheet and Powercard. DM should choose corresponding template for each new Actor:

![template](images for readme/0.png)

PC:

![pc_tab_1](images for readme/1.png)

Blue fields can not be modified directly and will be automatically calculated. 
Press Ability Score name (STR, CON, DEX etc, red box 1) to roll 1d20 + mod + 1/2 lvl (red box 2).
Press Skill name to roll 1d20 + 5 (if correlated box checked) + Ability Score mod + 1/2 lvl + MISC field. 
Initiative - press the die icon to roll formula in the blue field (add BONUS if you need to modify it, there are some Sandbox reasons behind this).
If you add the Actor token to the Combat Tracker and press the die icon - initiative will be added to the tracker. 
Bloodied = 1/2 of max HP.

![pc_tab_2](images for readme/2.png)

Level is the only important field here. 

**Powercards.** They are Actors too. So, DM need to create a folder and a bunch of powercards actor sheets for every player (and give them Owner permissions). 


![powercard](images for readme/3.png)

TYPE and DEFENSE fields will affect roll text. 
Press the die icon to roll the corresponding formula, which you should enter in standard Foundry format (1d20+123). 


## More warnings:

0. Do not touch TEMPLATES Actor folder and all existing Items, if you are not familiar with Sandbox.
1. Charsheet elements structure is a MESS. 
2. This system was created on Sandbox v0.3.9 and FoundryVTT v0.6.6. Foundry updates have a 99% chance of breaking everything, Sandbox updates have a 69% chance of breaking smth. 
3. I can add a bit more automation to the character sheet, but after discussion with my friends I decided, that 4e is too heavy (math and charsheet layout) to fully recreate it in the Sandbox, so I decided to keep this version small and simple. 

If something does not work as it should, try to: 
1. Change actor (REAL actor, not TEMPLATE) template to default and back to pc/powercard.
2. Create a new Actor.
3. Ctrl+F5 (refresh page, delete the cache).

by N1kk1#5001 (Discord)
